import Cookies from 'js-cookie';
import { usersService } from '../api/services';
import { useRecoilState } from 'recoil';
import { userState } from '../state/userState';

export function useSignin() {
  const [, setUser] = useRecoilState(userState);

  const setAccessToken = (accessToken: string) => {
    Cookies.set('accessToken', accessToken, { expires: 7 });
  };

  const setUserToStorage = () => {
    return usersService.getMe().then(({ data: user }) => {
      setUser(user);
    });
  };

  return { setAccessToken, setUserToStorage };
}
