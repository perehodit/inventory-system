import { ChakraProvider } from '@chakra-ui/react';

import SigninPage from './pages/Signin.page';

function App() {
  return (
    <ChakraProvider>
      <SigninPage />
    </ChakraProvider>
  );
}

export default App;
