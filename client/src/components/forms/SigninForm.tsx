import {
  Box,
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  InputGroup,
  InputRightElement,
  useToast,
} from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { LockClosedIcon, LockOpen1Icon } from '@radix-ui/react-icons';
import * as yup from 'yup';
import { KeyboardEvent, useState } from 'react';
import { authService } from '../../api/services';
import { useSignin } from '../../hooks';

const schema = yup
  .object({
    email: yup.string().email('Неверная почта').required('Введите почту'),
    password: yup.string().required('Введите пароль'),
  })
  .required();

type FormValues = {
  email: string;
  password: string;
};

export function SigninForm() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormValues>({ resolver: yupResolver(schema) });

  const { setAccessToken, setUserToStorage } = useSignin();

  const toast = useToast();

  const onSubmit = handleSubmit((data) => {
    authService.signin(data).then(({ data }) => {
      setAccessToken(data.accessToken);
      setUserToStorage();
    });
  });

  const onEnterClick = (event: KeyboardEvent<HTMLDivElement>) => {
    if (event.code === 'Enter' || event.code === 'NumpadEnter') {
      onSubmit();
    }
  };

  const [showPassword, setShowPassword] = useState<boolean>(false);

  return (
    <Box>
      <Box mb={5} onKeyDown={onEnterClick}>
        <FormControl isInvalid={errors.email ? true : false} mb={4}>
          <FormLabel>Почта</FormLabel>
          <Input placeholder="example@mail.ru" {...register('email')} />
          <FormErrorMessage>{errors.email?.message}</FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={errors.password ? true : false}>
          <FormLabel>Пароль</FormLabel>
          <InputGroup size="md">
            <Input
              placeholder="Пароль"
              {...register('password')}
              pr="4.5rem"
              type={showPassword ? 'text' : 'password'}
              inputMode="text"
            />
            <InputRightElement width="4.5rem">
              <Button
                h="1.75rem"
                size="sm"
                onClick={() => setShowPassword(!showPassword)}
              >
                {showPassword ? <LockOpen1Icon /> : <LockClosedIcon />}
              </Button>
            </InputRightElement>
          </InputGroup>
          <FormErrorMessage>{errors.password?.message}</FormErrorMessage>
        </FormControl>
      </Box>
      <Button w="100%" onClick={onSubmit} colorScheme="teal">
        Войти
      </Button>
    </Box>
  );
}
