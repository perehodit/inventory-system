import { api } from '../axios';

export const authService = {
  signin(data: { email: string; password: string }) {
    return api.post('/auth/signin', data);
  },
};
