import { api } from '../axios';

export const usersService = {
  getMe() {
    return api.get('/users/me');
  },
};
