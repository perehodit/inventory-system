import { atom } from 'recoil';

type UserState = {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  isAdministrator: string;
};

export const userState = atom<UserState | undefined>({
  key: 'User',
  default: undefined,
});
