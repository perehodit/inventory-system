import { Box, Heading } from '@chakra-ui/react';
import { SigninForm } from '../components/forms/SigninForm';

export default function SigninPage() {
  return (
    <Box h="100%" display="flex" justifyContent="center">
      <Box mt="26vh" w="100%" padding={5} maxWidth={350}>
        <Heading mb={6}>Вход в систему</Heading>
        <SigninForm />
      </Box>
    </Box>
  );
}
